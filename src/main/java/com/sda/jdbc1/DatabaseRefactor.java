package com.sda.jdbc1;

import java.sql.*;

public class DatabaseRefactor {
    private static final String URL = "jdbc:mysql://localhost:3306/university"; // specificam prima data driverul, dupa ':' tipul de baze de date folosit, ':' inca o data
    private static final String USER = "root";
    private static final String PASSWORD = "parola";

    private Connection connection;

    public DatabaseRefactor() throws SQLException {
        connection = DriverManager.getConnection(URL, USER, PASSWORD);
    }


    public void findAllStudents() throws SQLException {

        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery("SELECT * FROM students"); // result set tine rezultatul dat de query-ul nostru
        displayDataFromResultSet(resultSet);

    }

    public void findByFirstName(String name, String lastNames) throws SQLException {

        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery("SELECT * FROM students WHERE first_name =\"" + name + "\" and last_name =\"" + lastNames + "\""); // result set tine rezultatul dat de query-ul nostru
        displayDataFromResultSet(resultSet);

    }

    public void findByFirstNameAndLastNameSecured(String firstNameInput, String lastNameInput) throws SQLException {

        PreparedStatement statement = connection.prepareStatement("select * from students where first_name = ? and last_name = ?");
        statement.setString(1, firstNameInput);
        statement.setString(2, lastNameInput);
        ResultSet resultSet = statement.executeQuery();
        displayDataFromResultSet(resultSet);

    }

    public void insertStudent(String writeFirstName, String writeLastName, String writeEmail, Integer writeAge) throws SQLException {

        String inserQuery = "Insert into students (first_name,last_name,email,age) values (?,?,?,?)";
        PreparedStatement statement = connection.prepareStatement(inserQuery);
        setNewParameters(writeFirstName, writeLastName, writeEmail, writeAge, statement);
        int status = statement.executeUpdate();
        System.out.println("Number of rows affected (" + status + ").");

    }

    private void setNewParameters(String writeFirstName, String writeLastName, String writeEmail, Integer writeAge, PreparedStatement statement) throws SQLException {
        statement.setString(1, writeFirstName);
        statement.setString(2, writeLastName);
        statement.setString(3, writeEmail);
        statement.setInt(4, writeAge);
    }

    public void updateStudent(Integer id, String firstNameInput, String lastNameInput, String email, Integer age) throws SQLException {
        String updateQuery = "update students set first_name = ?, last_name = ?, email = ?, age = ? where id = ?";

        PreparedStatement statement = connection.prepareStatement(updateQuery);
        setNewParameters(firstNameInput, lastNameInput, email, age, statement);
        statement.setInt(5, id);
        statement.executeUpdate();
    }

    public void deleteStudent(int id) {
        String deleteQuery = "delete from students where id = ?";
        try {
            PreparedStatement statement = connection.prepareStatement(deleteQuery);
            statement.setInt(1, id);
            int status = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void displayDataFromResultSet(ResultSet resultSet) throws SQLException {
        while (resultSet.next()) {
            String firstName = resultSet.getString("first_name");
            String lastName = resultSet.getString("last_name");
            System.out.println(firstName + " " + lastName);
        }
    }
}
