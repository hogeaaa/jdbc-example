package com.sda.jdbc1;

public class Main {
    public static void main(String[] args) {
        Database database = new Database();
        database.createConnection();
        //database.findAllStudents();
        // database.findByFirstName("Ion","Ionescu \" or 1=1#");
        //database.findByFirstNameAndLastNameSecured("Ion","Ionescu \" or 1=1#"); // not gonna run
        //database.findByFirstNameAndLastNameSecured("Ion","Ionescu");
        //database.insertStudent("Andrei","Hogea","andrei@yahoo.com",22);
        //database.updateStudent(3,"Hogea","andrei","hogea@yaoo.com",30);
        //database.deleteStudent(4);
        StudentService studentService = new StudentService();
        studentService.action();
    }
}
