package com.sda.jdbc1;

import java.sql.*;

public class Database {
    private static final String URL = "jdbc:mysql://localhost:3306/university"; // specificam prima data driverul, dupa ':' tipul de baze de date folosit, ':' inca o data
    private static final String USER = "root";
    private static final String PASSWORD = "parola";

    private Connection connection;

    public void createConnection(){
        try {
            connection = DriverManager.getConnection(URL,USER,PASSWORD);
        } catch (SQLException e) {
            System.out.println("Cannot connect to the database.");
        }
    }

    public void findAllStudents(){
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM students"); // result set tine rezultatul dat de query-ul nostru
            while (resultSet.next()){
                String firstName = resultSet.getString("first_name");
                String lastName = resultSet.getString("last_name");
                System.out.println(firstName + " " + lastName);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void findByFirstName(String name, String lastNames){
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM students WHERE first_name =\"" + name + "\" and last_name =\"" + lastNames + "\""); // result set tine rezultatul dat de query-ul nostru
            while (resultSet.next()){
                String firstName = resultSet.getString("first_name");
                String lastName = resultSet.getString("last_name");
                System.out.println(firstName + " " + lastName);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void findByFirstNameAndLastNameSecured(String firstNameInput, String lastNameInput){
        try {
            PreparedStatement statement = connection.prepareStatement("select * from students where first_name = ? and last_name = ?");
            statement.setString(1,firstNameInput);
            statement.setString(2,lastNameInput);
            ResultSet resultSet = statement.executeQuery();
            while(resultSet.next()){
                String firstName = resultSet.getString("first_name");
                String lastName = resultSet.getString("last_name");
                System.out.println(firstName + " " + lastName);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void insertStudent(String writeFirstName, String writeLastName,String writeEmail, Integer writeAge){
        try {
            String inserQuery = "Insert into students (first_name,last_name,email,age) values (?,?,?,?)";
            PreparedStatement statement = connection.prepareStatement(inserQuery);
            statement.setString(1,writeFirstName);
            statement.setString(2,writeLastName);
            statement.setString(3,writeEmail);
            statement.setInt(4,writeAge);
            int status = statement.executeUpdate();
            System.out.println("Number of rows affected (" + status + ").");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void updateStudent(Integer id,String firstNameInput, String lastNameInput, String email, Integer age){
        String updateQuery = "update students set first_name = ?, last_name = ?, email = ?, age = ? where id = ?";
        try {
            PreparedStatement statement = connection.prepareStatement(updateQuery);
            statement.setString(1,firstNameInput);
            statement.setString(2,lastNameInput);
            statement.setString(3,email);
            statement.setInt(4,age);
            statement.setInt(5,id);
            statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void deleteStudent(int id){
        String deleteQuery ="delete from students where id = ?";
        try {
            PreparedStatement statement = connection.prepareStatement(deleteQuery);
            statement.setInt(1,id);
            int status = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
