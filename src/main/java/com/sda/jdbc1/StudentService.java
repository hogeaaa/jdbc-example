package com.sda.jdbc1;

import java.sql.SQLException;
import java.util.Scanner;

public class StudentService {

    private DatabaseRefactor database;
    private Scanner scanner;
    boolean isRunning;

    public StudentService(){
        scanner = new Scanner(System.in);
        isRunning = true;
    }

    public void action(){
        try {
            database = new DatabaseRefactor();
            while (isRunning) {
                Scanner scanner = new Scanner(System.in);
                System.out.println("Enter command ");
                Integer command = scanner.nextInt();
                System.out.print("\033[H\033[2J");
                System.out.flush();
                executeCommand(command);
            }
        } catch (SQLException e) {
            System.out.println("Go to sql exception");
        }
    }

    public void executeCommand (Integer command) throws SQLException{
        switch (command){
            case 1:
                System.out.println("This will insert a student");
                System.out.println("Insert first name of the student: ");
                String firstName = scanner.next();
                System.out.println("Insert the last name of the student: ");
                String lastName = scanner.next();
                System.out.println("Insert the email of the student: ");
                String email = scanner.next();
                System.out.println("Insert the age of the student: ");
                Integer age = scanner.nextInt();
                database.insertStudent(firstName,lastName,email,age);

                break;
            case 2:
                System.out.println("This will select a student");
                System.out.println("Insert the first name of the student: ");
                String firstNameSelect = scanner.next();
                System.out.println("Insert the last name of the student: ");
                String lastNameSelect = scanner.next();
                database.findByFirstNameAndLastNameSecured(firstNameSelect,lastNameSelect);
                break;
            case 3:
                System.out.println("This will update a student.");
                System.out.println("Insert the id of the student you want to modify: ");
                Integer idUpdate = scanner.nextInt();
                System.out.println("Insert the first name of the student: ");
                String firstNameUpdate = scanner.next();
                System.out.println("Insert the last name of the student you want to modify");
                String lastNameUpdate = scanner.next();
                System.out.println("Insert the email of the student you want to modify");
                String emailUpdate = scanner.next();
                System.out.println("Insert the age of the student you want to modify: ");
                Integer ageUpdate = scanner.nextInt();
                database.updateStudent(idUpdate,firstNameUpdate,lastNameUpdate,emailUpdate,ageUpdate);
                break;
            case 4:
                System.out.println("This will delete a student.");
                System.out.println("Insert the id of the student you want to delete");
                Integer id = scanner.nextInt();
                database.deleteStudent(id);
                break;

            default:
                isRunning = false;
                System.out.println("Goodbye!!");
        }
    }
}
